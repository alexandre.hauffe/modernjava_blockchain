package blockChainLibrary;

import lombok.Getter;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedList;
import java.util.List;

@Slf4j
@Getter
@Value
public class BlockChain{

    List<Block> blockChain = new LinkedList<>();
    int prefix = 4;
    String prefixString = new String(new char[prefix]).replace('\0', '0');

    public synchronized void addBlock(Block block, String miner){
        if(blockChain.isEmpty()
                || (block.getPreviousHash()!=null && block.getPreviousHash().equals(getLastHash()))
                && block.getHash().equals(block.calculateBlockHash())
                && block.getHash().substring(0, prefix).equals(prefixString)){

            blockChain.add(block);
            log.warn(miner + " added a new block: \n" + block);
        }
    }

    public synchronized String getLastHash(){
        if(!blockChain.isEmpty())
            return blockChain.get(blockChain.size()-1).getHash();
        else
            return null;
    }
}
